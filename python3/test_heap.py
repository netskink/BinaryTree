"""Execute SQL on multiple servers
"""

from ns_Heap import ns_Heap



print("Init heap with presorted list having max heap property.")
X = ns_Heap([16, 14, 10, 8, 7, 9, 3, 2, 4, 1])

# a list which is not setup with max heap property
#X = ns_Heap([1, 2, 3, 4, 7, 8, 9, 10, 14, 16])


#print("Init heap with [1,3,5]")
#x = ns_Heap([5,3,1])


print("X is %s" % (X))
print("size of X is %s" % (X.size()))

print("do parent for each node from 2 to 0 index")
print(X.parent(2))
print(X.parent(1))
#print(x.parent(0))


print("do left for each node from 2 to 0 index")
print(X.left(2))
print(X.left(1))
#print(x.left(0))


print("do right for each node from 2 to 0 index")
print(X.right(2))
print(X.right(1))
#print(x.right(0))

print("max_heapify from position 1, the root")
X.max_heapify(1)
print("X is %s" % (X))
print("exercise the build_max_heap routing")
X.build_max_heap()
print("X is %s" % (X))


#print("Add some even values: 6")

#x.add(6)

#print("x is %s" % (x))


#print("Add some even values: 0")

#x.add(0)

#print("x is %s" % (x))

#print("Add some even values: 2")

#x.add(2)

#print("x is %s" % (x))

print("max is %d" % (X.peek_max()))
print("extrat_max returns %d" % (X.extract_max()))
print("x is %s" % (X))

print("continue to extract heap until empty.")
while X.heap_size !=0:
    print("extrat_max returns %d" % (X.extract_max()))
    print("x is %s" % (X))

print("******X is empty again ***********")

print("Add key 1")
X.insert(1)
print("x is %s" % (X))
print("Add key 2")
X.insert(2)
print("x is %s" % (X))
print("Add key 3")
X.insert(3)
print("x is %s" % (X))
print("Add key 4")
X.insert(4)
print("x is %s" % (X))
print("Add key 5")
X.insert(5)
print("x is %s" % (X))

print("continue to extract heap until empty.")
while X.heap_size !=0:
    print("extrat_max returns %d" % (X.extract_max()))
    print("x is %s" % (X))

print("******X is empty again ***********")


print("Add key 1")
X.insert(1)
print("x is %s" % (X))
print("Add key 2")
X.insert(2)
print("x is %s" % (X))
print("Add key 3")
X.insert(3)
print("x is %s" % (X))
print("Add key 4")
X.insert(4)
print("x is %s" % (X))
print("Add key 5")
X.insert(5)
print("x is %s" % (X))

print("increase last child by increasing values to make it a new max")
print("Do this for all five values")
a_value = 6
while a_value < 11:
    print("increase index 5 to %d" % (a_value))
    X.increase_value(5,a_value)
    print("x is %s" % (X))
    a_value = a_value + 1


print("continue to extract heap until empty.")
while X.heap_size !=0:
    print("extrat_max returns %d" % (X.extract_max()))
    print("x is %s" % (X))

print("******X is empty again ***********")


print("Add key 1")
X.insert(1)
print("x is %s" % (X))
print("Add key 2")
X.insert(2)
print("x is %s" % (X))
print("Add key 3")
X.insert(3)
print("x is %s" % (X))
print("Add key 4")
X.insert(4)
print("x is %s" % (X))
print("Add key 5")
X.insert(5)
print("x is %s" % (X))

print("parent of each index.")
a_index = 1
while a_index <= X.size():
    print("parent(%d) is  %d" % (a_index, X.parent(a_index)))
    a_index = a_index + 1

# with no parms it starts at the root node
X.walk_tree()
