# a max heap priority queue implementation for use with micropython

"""Code for a simple max heap.  This algorithm came from the CERS book.
   A max heap is simply each node has the max value at its root and each
   node of the subtree maintains the same property.

	Here is an example tree of the values 1-5

                            5
                           / \
                          /   \
                         4     2
                        / \   
                       1   3
"""


import time

class ns_PriorityNode(object):
	""""
	Attributes:
		node: has a value and object
	"""
	def __init__(self, priority, a_thing):
		if priority is None:
			return
		self.priority = priority
		self.a_thing = a_thing

	def priority(self):
		return self.priority

	def __repr__(self):
		a_string = '\r\n  ns_PriorityNode: \tpriority={}\r\n\t\t\ta_thing={!r}\r\n'.format(self.priority,self.a_thing)
		return a_string


class ns_PriorityQueue(object):
	"""
	Attributes:
		heap: List representation of a max heap
	Implementation Notes:
		The heap is implemented as a list.  The algorithm uses list indexes
        counting from 1 to n.  Where the size of the heap is n.  The list
		has a null data values at the normal list position of 0.  The heap is 
        sorted so that the root containing the max value is position 1.  The numbering
        continues so that left is at postion 2 and right is at position 3. Likewise
        the left and right of node 2 is 4 and 5.  Hence left = 2i and right is 2*i + 1.
        This relationship is why list position 0 is not used.
	"""
	def __init__(self, heap=None):
		# the algo book uses indexes which start at 1 instead of 0
		# add a dummy value at the first item
		self.heap_list = [ns_PriorityNode(-3367,'xxx')]
		if heap is None:
			return

		# append the list parameter after the dummy value	
		self.heap_list.extend(heap)

		# we will not use len() of the list but instead use size
		self.heap_size = len(self.heap_list) - 1

		# Go ahead do make sure max heap property is applied
		self.build_max_heap()
		

	# Used to print/dump the heap as a list
	def __repr__(self):
		return 'ns_Heap({!r})'.format(self.heap_list[1:])

	def size(self):
		return self.heap_size

	def parent(self, i):
		if i < 1:
			assert 0, "heap_index_min"
		if i > self.heap_size:
			assert 0, "heap_index_max"

		return i // 2

	def left(self, i):
		if i < 1:
			assert 0, "heap_index_min"
		if i > self.heap_size:
			assert 0, "heap_index_max"

		return 2 * i

	def right(self, i):
		if i < 1:
			assert 0, "heap_index_min"
		if i > self.heap_size:
			assert 0, "heap_index_max"

		return 2 * i + 1

	def max_heapify(self,i):
		"""max heaps from index i

		:param self: the heap containing the node to max heap
		:type name: ns_Heap.
		:param i: index of a node in the binary tree where max heap occurs
		:type state: int.
		:returns:  nothing -- nothing
		:raises: heap_index_min, heap_index_max

		"""
		if i < 1:
			assert 0, "heap_index_min"
		if i > self.heap_size:
			assert 0, "heap_index_max"

		left_index = self.left(i)
		right_index = self.right(i)

		if left_index <= self.heap_size and \
			self.heap_list[left_index].priority > self.heap_list[i].priority:
			largest_index = left_index
		else:
			largest_index = i

	
		if right_index <= self.heap_size and \
			self.heap_list[right_index].priority > self.heap_list[largest_index].priority:
			largest_index = right_index

		if largest_index != i:
			# swap heap_list[i] with heap_list[largest]
			temp_value = self.heap_list[i]
			self.heap_list[i] = self.heap_list[largest_index]
			self.heap_list[largest_index] = temp_value
			self.max_heapify(largest_index)

	def build_max_heap(self):
		
		for i in range(self.heap_size//2, 0, -1):	# can also wrap with reversed()
			self.max_heapify(i)

	def peek_max(self):
		""" Returns the node with max priority

		:returns:  ns_PriorityQueue_Node
		:raises: heap empty 
		"""	
		if self.heap_size < 1:
			assert 0, "heap_empty"

		return self.heap_list[1]

	def extract_max(self):
		if self.heap_size < 1:
			assert 0, "heap_underflow"

		the_max = self.heap_list[1]
		if self.heap_size == 1:
			# pop and toss the last one
			self.heap_list.pop()
			self.heap_size = 0
		else:	
			self.heap_list[1] = self.heap_list.pop()
			self.heap_size = len(self.heap_list) - 1
			self.max_heapify(1)
		return the_max

	def increase_value(self, i, priority):
		""" for a given index, modify the key/value to be larger.
			Afterwards, reflow the heap with modified key.
			Keep in mind, the index is one based.

		:param i: the index to increase priority value
		:type i: integer.
		:param priority: value
		:type int: the value of the new priority specified by index i
		:returns:  nothing -- nothing
		:raises: value/key is less than existing value 
		"""	

		if priority < self.heap_list[i].priority:
			assert 0, "value/key is less than existing value"

		self.heap_list[i].priority = priority
		while i > 1 and self.heap_list[self.parent(i)].priority < self.heap_list[i].priority:
			# swap heap_list[i] with heap_list[parent(i)]
			temp_node = self.heap_list[i]
			self.heap_list[i] = self.heap_list[self.parent(i)]
			self.heap_list[self.parent(i)] = temp_node
			i = self.parent(i)


	def insert(self, priority_node):
		self.heap_size = self.heap_size + 1
		#tic = time.perf_counter()
		# method #1 (me)
		#self.heap_list.append(value)
		#self.build_max_heap()
		# method #2 (book) *** This is faster ***
		# set the intial value to be -10000 and then use
		# increase value routine to adjust priority and reflow heap
		self.heap_list.append(ns_PriorityNode(-100000,priority_node.a_thing)) 
		self.increase_value(self.heap_size, priority_node.priority)
		#toc = time.perf_counter()
		#print("execution time is %f" % (toc - tic))
		# no need if doing method 2 self.build_max_heap()
		

	def walk_tree(self, i=1):
		""" Visits each node of the tree.
			
			Starts with root node, then goes left path to leaf
			then returns to last non leaf node and does right path.		
			After this node it will start again on left path.  Basically
			it visits each node in sorted order if the heap was sorted.
		

		:param i: the index to start node walk. Defaults to first, the root node.
		:type i: integer.
		:returns:  nothing -- nothing
		:raises: none
		"""	
		if i < 1:
			return

		if i > self.heap_size:
			return

		depth = 0
		current = self.parent(i)
		while (current > 0):
			depth = depth + 1
			current = self.parent(current)

		print("Node (depth:{}) is : {}".format(depth,self.heap_list[i]))


		self.walk_tree(self.left(i))
		self.walk_tree(self.right(i))



