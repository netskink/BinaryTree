"""Execute SQL on multiple servers
"""

from ns_PriorityQueue import ns_PriorityQueue
from ns_PriorityQueue import ns_PriorityNode

# Create a list of ns_PriorityQueueNodes with random order

# Create a list with presorted list having max heap property
x_list = [ns_PriorityNode(1, 'one'),
          ns_PriorityNode(4, 'four'),
          ns_PriorityNode(2, 'two'),
          ns_PriorityNode(5, 'five'),
          ns_PriorityNode(3, 'three')]
print("x_list = %s" % (x_list))


print("Init heap with rando ordered list.")
X = ns_PriorityQueue(x_list)




# Create a list of ns_PriorityQueueNodes with the priority set so 
# that the list is in order for insert as is with max heap proprty
# 5 4 2 1 3

# Create a list with presorted list having max heap property
#x_list = [ns_PriorityNode(5, 'five'),
#          ns_PriorityNode(4, 'four'),
#          ns_PriorityNode(2, 'two'),
#          ns_PriorityNode(1, 'one'),
#          ns_PriorityNode(3, 'three')]
#print("x_list = %s" % (x_list))
#print("Init heap with presorted list having max heap property.")
#X = ns_PriorityQueue(x_list)
#print("X is %s" % (X))


print("size of X is %s" % (X.size()))

print("do parent for each node from 2 to 0 index")
print(X.parent(2))
print(X.parent(1))
#print(X.parent(0))


print("do left for each node from 2 to 0 index")
print(X.left(2))
print(X.left(1))
#print(X.left(0))


print("do right for each node from 2 to 0 index")
print(X.right(2))
print(X.right(1))
#print(X.right(0))

print("max_heapify from position 1, the root")
X.max_heapify(1)
print("X is %s" % (X))

print("exercise the build_max_heap routing")
X.build_max_heap()
print("X is %s" % (X))


print("Add/insert some even values: 6")
X.insert(ns_PriorityNode(6, 'six'))
print("X is %s" % (X))


print("Add/insert some even values: 8")
X.insert(ns_PriorityNode(8, 'eight'))
print("X is %s" % (X))


print("Add/insert some even values: 10")
X.insert(ns_PriorityNode(10, 'ten'))
print("X is %s" % (X))


print("max is {}".format (X.peek_max()))
print("X is %s" % (X))

print("continue to extract heap until empty.")
while X.heap_size !=0:
    print("***** extract_max returns {}".format(X.extract_max()))
    print("X is %s" % (X))

print("******X is empty again ***********")

# Create a list with presorted list having max heap property
x_list = [ns_PriorityNode(5, 'five'),
          ns_PriorityNode(4, 'four'),
          ns_PriorityNode(2, 'two'),
          ns_PriorityNode(1, 'one'),
          ns_PriorityNode(3, 'three')]
print("x_list = %s" % (x_list))
print("Init heap with presorted list having max heap property.")
X = ns_PriorityQueue(x_list)
print("continue to extract heap until empty.")
while X.heap_size !=0:
    print("***** extract_max returns {}".format(X.extract_max()))
    print("X is %s" % (X))

print("******X is empty again ***********")

# add five values in random order

print("Add/insert some values: 3")
X.insert(ns_PriorityNode(3, 'three'))
print("X is %s" % (X))

print("Add/insert some values: 1")
X.insert(ns_PriorityNode(1, 'one'))
print("X is %s" % (X))

print("Add/insert some values: 5")
X.insert(ns_PriorityNode(5, 'five'))
print("X is %s" % (X))

print("Add/insert some values: 4")
X.insert(ns_PriorityNode(4, 'four'))
print("X is %s" % (X))

print("Add/insert some values: 2")
X.insert(ns_PriorityNode(2, 'two'))
print("X is %s" % (X))

#print("increase last child by increasing values to make it a new max")
#print("Do this for all five values")
#a_value = 6
#while a_value < 11:
#    print("increase index 5 to %d" % (a_value))
#    X.increase_value(5,a_value)
#    print("x is %s" % (X))
#    a_value = a_value + 1
#
#
#print("continue to extract heap until empty.")
#while X.heap_size !=0:
#    print("extrat_max returns %d" % (X.extract_max()))
#    print("x is %s" % (X))
#
#print("******X is empty again ***********")
#
#
#print("Add key 1")
#X.insert(1)
#print("x is %s" % (X))
#print("Add key 2")
#X.insert(2)
#print("x is %s" % (X))
#print("Add key 3")
#X.insert(3)
#print("x is %s" % (X))
#print("Add key 4")
#X.insert(4)
#print("x is %s" % (X))
#print("Add key 5")
#X.insert(5)
#print("x is %s" % (X))
#
#print("parent of each index.")
#a_index = 1
#while a_index <= X.size():
#    print("parent(%d) is  %d" % (a_index, X.parent(a_index)))
#    a_index = a_index + 1
#
## with no parms it starts at the root node
print("Walk the tree")
X.walk_tree()
